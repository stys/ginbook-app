var app = angular.module('ginbookApp', []);

/* Define constants */
app.constant("activeClass", "btn-primary");

/* Top Application Controller */
app.controller('TopCtrl', function($scope, $http, $filter, activeClass) {
	
	$scope.screen = {
		"dashboard": false,
		"modules": true	
	}
	
	$scope.setScreen = function(scr) {
		if( angular.isDefined($scope.screen[scr]) ) {
			for(var s in $scope.screen) {
				$scope.screen[s] = false;
			}
			$scope.screen[scr] = true;
		}
	}
	
	/* Data API */	
	$scope.data = {
		
		/* Modules */		
		getModules: function() {
			if($scope.data.modules) {
				return Object.keys($scope.data.modules);
			}
		},
		addModule: function(module) {
			if($scope.data.modules) {
				$scope.data.modules[module] = true;
			}
		},
		
		/* Groups */
		getGroups: function() {
			if($scope.data.groups) {
				return Object.keys($scope.data.groups);
			}
		},
		addGroup: function(group) {
			if($scope.data.groups && !angular.isDefined($scope.data.groups[group])) {
				$scope.data.groups[group] = true;
			}
		}
	}
	
	/* Get initial data from server */
	$http.get('http://localhost:9090/data?file=data.json')
		.success(function(data) {
			/* Set model */
			angular.extend($scope.data, data);
		})
	    .error(function(err) {
			alert(JSON.stringify(err, null, 4));
		});
			
	/* Save button click handler: save rules to server */
	$scope.save = function() {
		$http.post('http://localhost:9090/data?file=data.json', $scope.data);
	}	
});

app.controller("ModulesController", function($scope, activeClass) {
	
	/* Module selection support */
	$scope.selectedModule = null;
	
	$scope.selectModule = function(module) {
		$scope.selectedModule = module;
	}
	
	$scope.getModuleActiveClass = function(module) {
		return $scope.selectedModule == module ? activeClass : " ";
	}
		
	$scope.groupModuleFilterFn = function(group) {
		if($scope.selectedModule && $scope.data.groups) {
			if($scope.data.groups[group] && $scope.data.groups[group].modules) {
				return $scope.data.groups[group].modules.find(function(module) {
					return module.module == $scope.selectedModule;	
				});
			}
		}
		return false;
	}
	
	/* Group selection support */
	$scope.selectedGroup = null;
	
	$scope.selectGroup = function(group) {
		$scope.selectedGroup = group;
	}
	
	$scope.getGroupActiveClass = function(group) {
		return $scope.selectedGroup == group ? activeClass : " ";
	}
	
	/* Group creation support */
	$scope.processForm = function() {
		alert($scope.selectedModule);
		$scope.data.addGroup($scope._txt);
	}
});